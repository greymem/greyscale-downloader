# GreyScaleDownloader 
Приложение для скачивания картинок и сохранения в оттенках серого.

Использование

```
greyscale-downloader.jar <PATH> <width> <height>
```

Поддерживаемые форматы изображений: bmp, jpg, jpeg, gif, png