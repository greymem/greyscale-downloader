
/*
 * ====================================================================
 *
 *  GVA 2019
 */
package ru.opensoftdev.greyscaledownloader;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

public class Application {
	private static final String HTTP_REGEX = "^((http(s)?)|(file)):.*";
	private static final String IMAGE_EXT_REGEX = ".*\\.(bmp|jpg|jpeg|gif|png)$";
	
	public static void main(String[] args) {
		Application app = new Application();
		if (args==null || args.length<3 || args.length>3) {
			System.err.println("There must be 3 arguments");
			return;
		}
		
		try {
			String resultPath = app.getPathToGrayscaledImg(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]));
			System.out.println("Operation complete. Path to the new file:"+resultPath);
		} catch (Exception e) {
			System.err.println("Can't process file '"+args[0]+"'. Reason: "+e.getMessage());
		}
	}
	
	public String getPathToGrayscaledImg(String path, int width, int height) throws IOException {
		checkParams(path, width, height);
		try (InputStream inputStream = openFile(path)) {
			BufferedImage sourceImage = ImageIO.read(inputStream);
			
			BufferedImage newImage = new BufferedImage( width, 
					height, 
                     BufferedImage.TYPE_BYTE_GRAY);
			
			Graphics2D graphics = newImage.createGraphics();
			graphics.drawImage(sourceImage.getScaledInstance(width, height, Image.SCALE_SMOOTH), 0, 0, null);
			graphics.dispose();
			
			
			String format = getImgFormat(path);
			StringBuilder newFileName = new StringBuilder("img").append("_").append((Math.random() * 100)).append(".")
					.append(format);
			File newFile = new File(newFileName.toString());
			ImageIO.write(newImage, format, newFile);
			
			String newFilePath = newFile.getAbsolutePath();
			System.out.println(
					String.format("New image saved. File path: %s (Width %s, Height %s)", newFilePath, width, height));
			return newFilePath;
		}
	}
	
	private static void checkParams(String path, int width, int heights) {
		if (width<=0) {
			throw new IllegalArgumentException("Wrong width parameter '"+width+"'");
		}
		if (heights<=0) {
			throw new IllegalArgumentException("Wrong heights parameter '"+heights+"'");
		}
		
		final Pattern pattern = Pattern.compile(IMAGE_EXT_REGEX, Pattern.CASE_INSENSITIVE);
		
		if (!pattern.matcher(path).matches()) {
			throw new IllegalArgumentException("Wrong file extension '"+path+"'");
		}
	}
	
	private static InputStream openFile(String path) throws IOException {
		InputStream stream = null;
		
		final Pattern pattern = Pattern.compile(HTTP_REGEX, Pattern.CASE_INSENSITIVE);
		
		if (pattern.matcher(path).matches()) {
			stream = new URL(path).openStream();
		} else {
			stream = new FileInputStream(new File(path));
		}
		return stream;
	}
	
	private static String getImgFormat(String path) {
		final Pattern pattern = Pattern.compile(IMAGE_EXT_REGEX, Pattern.CASE_INSENSITIVE);
		Matcher extMatcher = pattern.matcher(path);
		extMatcher.find();
		return extMatcher.group(1);
	}
}
