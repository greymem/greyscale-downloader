import static org.junit.Assert.*;

import org.junit.Test;

import ru.opensoftdev.greyscaledownloader.Application;

public class ComplexTest {
	private static final String LOCAL_FILE = "C:\\Temp\\agoda.PNG";
	private static final String REMOTE_HTTP_FILE = "https://www.baeldung.com/wp-content/uploads/2018/10/rws-icon-home.jpg";
	private static final String FILE_WITH_FILE_PROTOCOL = "file:///C:/Temp/agoda.PNG";
	
	@Test
	public void processLocalFile() {
		Application app = new Application();
		try {
			String result = app.getPathToGrayscaledImg(LOCAL_FILE, 500, 500);
			assertTrue(result instanceof String);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	

	@Test
	public void processFileOverHttp() {
		Application app = new Application();
		try {
			String result = app.getPathToGrayscaledImg(
					REMOTE_HTTP_FILE, 150, 100);
			assertTrue(result instanceof String);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void processFileWithFileProtocol() {
		Application app = new Application();
		try {
			String result = app.getPathToGrayscaledImg(FILE_WITH_FILE_PROTOCOL, 200, 200);
			assertTrue(result instanceof String);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	@Test
	public void checkWrongFileExtension() {
		Application app = new Application();
		try {
			String result = app.getPathToGrayscaledImg("file:///C:/Temp/agoda.txt", 300, 300);
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().contains("Wrong file extension"));
		}
	}
	
	@Test
	public void checkWrongWidth() {
		Application app = new Application();
		try {
			String result = app.getPathToGrayscaledImg(LOCAL_FILE, 0, 225);
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().contains("Wrong width parameter"));
		}
	}
	
	@Test
	public void checkWrongHeights() {
		Application app = new Application();
		try {
			String result = app.getPathToGrayscaledImg(LOCAL_FILE, 180, -25);
			fail();
		} catch (Exception e) {
			assertTrue(e.getMessage().contains("Wrong heights parameter"));
		}
	}
	
}
